"""
# TIPOS DE DATOS
numero_entero =10
print(numero_entero)
print(type(numero_entero))
numero_flotante=10.5
print(numero_flotante)
print(type(numero_flotante))
variable_booleana= False
print(variable_booleana)
print(type(variable_booleana))
variable_string = "hola a todos"
print(variable_string)
print(type(variable_string))
variable_lista = [1,"hola",False,3.5]
print("la variable lista es= ",variable_lista)
variable_tupla= (1,"hola")
print("la variable tupla contine: ",variable_tupla)
print("el primer elemento de la lista es: ", variable_lista[0])
variable_diccionario = {"uno":1,"dos":"hola","tres":False,"cuatro":3.5}
print("el diccionario: ",variable_diccionario)
print("el contenido de la llave uno es: ", variable_diccionario["uno"])
#print("la lista convertida en cadena es: ",variable_string.join(variable_lista))
"""
"""
#INPUT Y PRINT
x = int(input("ingrese un número: "))
print("el numero ingresado fue: ",x)
"""
"""
#OPERADORES ARITMETICOS
print("suma 2+2=", 2+2)
print("resta 5-3=", 5-3)
print("multiplicacion 5*3=",5*3)
print("division 5/3=",5/3)
print("division entera 5//3=",5//3)
print("modulo de division 5%3=",5%3)
print("potencia 2**3=",2**3)
"""
"""
#OPERADORES DE COMPARACION
print("5>3?",5>3)
print("5>=5?",5>=5)
print("5<2?",5<2)
print("hola==hola?","hola"=="hola")
print("hola!=hola?","hola"!="hola")
"""
"""
#OPERADORES LOGICOS
print("3>3 or 3>2 or 5>5?",3>3 or 3>2 or 5>5)
print("3>1 and 3>2 and 3>=3",3>1 and 3>2 and 3>=3)
print("not 3>1", not 3>1)
"""
"""
#IF - ELSE (BIFURCACIONES)
if(3>1 and 3>2 ):     
    if(3>3):
        print("la condicion se cumplió")
    else:
        print("no se cumplió")
        if(10>5):
            print("pero esta si se cumplió")
else:
    print("la condición no se cumplió")
"""
""" 
#USO DE IMPORT RANDOM
import random
print(random.randrange(1,4))
"""
""" 
#OPERACIONES DE ASIGNACION
numero = 2
print(numero)
numero+=2#numero = numero +2
print(numero)
"""
"""
#CICLO WHILE
numero = 1
while(numero<20):
    print("el numero tiene valor de: ",numero)
    numero+=1        
    if(numero>5 and numero<8):
        print("el numero puede ser seis o siete")
        while(numero<11):
            print("el numero dentro del while2 es: ",numero)
            numero+=1
print("al final el numero tiene el valor de: ",numero)
"""
""" 
#CICLO FOR
lista = [1,"dos","tres",4.5,False,[1,2,3],{"uno":"Enero","lista_dic":["febrero","Marzo"]}]
for elemento in lista:
    print(elemento)
print("======================")
for i in range(0,10): #i va ir desde cero hasta 9
    print(i)
print("===================")
for i in(range(0,len(lista))):
    elemento = lista[i]
    print("el elemento en la posición ",i," de la lista es: ",elemento)

meses = {"uno":"Enero","dos":"Febrero", "tres":"Marzo"}
for llave in meses:
    print(llave)

for valor in meses.values():
    print(valor)
"""
""" 
#SENTENCIAS BREAK Y CONTINUE
contador=0
while(contador<20):
    print(contador)
    contador+=1
    if(contador==7):
        continue
    if(contador==9):
        break

"""
""" 
import funciones

lista_numeros = [5,5,3,1,5]
media = funciones.calcular_media(lista_numeros)
print(media)
"""
try:
    numero = int(input("ingrese un número: "))
    print("el numero ingresado es: ",numero)
except:
    print("el formato ingresado no es valido")
    
print("aca hay un código importante para ser ejecutado")